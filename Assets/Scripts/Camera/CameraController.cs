﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public CameraNode startNode;
    public float speed;
    public float rotateSpeed;

    private CameraNode node;

    void Start()
    {
        SetNode(startNode);
        this.transform.position = startNode.transform.position;
        this.transform.rotation = startNode.transform.rotation;
    }

    public void SetNode(CameraNode _node)
    {
        if(_node != node)
            node = _node;
    }

    void Update()
    {

        if(Vector3.Distance(this.transform.position, node.transform.position) > 1e-8)
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, node.transform.position, speed * Time.deltaTime);
        }

        if(Quaternion.Angle(this.transform.rotation, node.transform.rotation) > 1e-8)
        {
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, node.transform.rotation, rotateSpeed * Time.deltaTime);
        }

    }

}