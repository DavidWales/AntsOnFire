﻿using UnityEngine;

public class CameraMoveTrigger : Trap
{
    public CameraNode cameraNode;
    private CameraController cameraController;

    void Awake()
    {
        cameraController = GameObject.FindObjectOfType<CameraController>();
    }

    protected override void OnTriggerEnterWithAnt(Collider collider)
    {
        if(collider.GetComponent<PlayerController>() != null)
        {
            cameraController.SetNode(cameraNode);
        }
    }
}