﻿public class Tags
{
    public static string FOLLOWER_ANTS_NODE = "FollowerAntsNode";
    public static string FIRE_NODE = "FireNode";
}

public class Prefs
{
    public static string NUM_ANTS = "NumAnts";
}

public class PlayerAnimatorParams
{
    public static string IS_WALKING = "IsWalking";
    public static string IS_DYING = "IsDying";
}

public class SlaveAnimatorParams
{
    public static string IS_WALKING = "IsWalking";
    public static string IS_DYING = "IsDying";
}

public class AnteaterAnimatorParams
{
    public static string IS_ATTACKING = "IsAttacking";
}