﻿public enum MouseButton
{
    LEFT_BUTTON = 0,
    RIGHT_BUTTON = 1,
    MIDDLE_BUTTON = 2
}

public enum SceneIndex
{
    SPLASH_SCREEN = 0,
    DAVID_TEST_SCENE = 1,
    JOSH_TEST_SCENE = 2,
    LEVEL_1 = 3,
    LEVEL_2 = 4,
    LEVEL_3 = 5,
    LEVEL_4 = 6,
    LEVEL_5 = 7,
    CINEMATIC_SCREEN = 8,
    VICTORY_LOSS_SCREEN = 9
}