﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowerAnt : MonoBehaviour
{
    public bool recruited = true;
    public float recruitmentRange = 5.0f;
    public float minDistanceToNextAnt = 1.0f;
    public float speed = 9.0f;
    public float turnSpeed = 10.0f;
	public AudioClip recruitedSound;
	public AudioClip deathSound;

	[HideInInspector]
	public GameObject nextAnt;
	[HideInInspector]
	public GameObject prevAnt;
	private FollowerAnt_State antState;
	private Animator followerAnimator;
    private Collider followerCollider;
    private Rigidbody followerRigidbody;

    public enum AntState
    {
        NOT_RECRUITED,
        RECRUITED,
        DEATH
    }

    void Awake()
    {
		followerAnimator = this.GetComponent<Animator>();
        followerCollider = this.GetComponent<Collider>();
        followerRigidbody = this.GetComponent<Rigidbody>();
    }

    void Start()
    {
        SetAntState(recruited ? AntState.RECRUITED : AntState.NOT_RECRUITED);
    }

    void Update()
    {
        antState.UpdateAnt();
    }

    public void SetNextAnt(GameObject ant)
    {
        nextAnt = ant;
    }

    public void SetAntState(AntState newState)
    {
        switch(newState)
        {
            case AntState.RECRUITED:
                antState = new FollowerAnt_Recruited(this);
                followerCollider.enabled = true;
                break;
            case AntState.NOT_RECRUITED:
                antState = new FollowerAnt_NotRecruited(this);
                followerCollider.enabled = false;
                break;
            case AntState.DEATH:
                antState = new FollowerAnt_DoNothing();
                followerCollider.enabled = false;
                followerRigidbody.freezeRotation = false;
                break;
            default:
                antState = new FollowerAnt_DoNothing();
                followerCollider.enabled = false;
                break;
        }
    }

	public void Die()
	{
        nextAnt = null;
        prevAnt = null;
        this.transform.parent = null;
        SetAntState(AntState.DEATH);
        followerAnimator.SetBool(SlaveAnimatorParams.IS_DYING, true);
		SoundManager.PlayOneShot(deathSound);
	}

    private IEnumerator DestroyAfterAnimation()
    {
        yield return new WaitUntil(() =>
        {
            return followerAnimator.GetBool(SlaveAnimatorParams.IS_DYING) == true && followerAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f;
        });

        Destroy(this.gameObject);
    }
}