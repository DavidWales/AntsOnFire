﻿using UnityEngine;

public class FollowerAnt_NotRecruited : FollowerAnt_State
{
    private FollowerAnt followerAnt;
    private PlayerController player;
    private Animator followerAnimator;

    public FollowerAnt_NotRecruited(FollowerAnt _followerAnt)
    {
        followerAnt = _followerAnt;
        player = GameObject.FindObjectOfType<PlayerController>();
        followerAnimator = followerAnt.GetComponent<Animator>();
        followerAnimator.SetBool(SlaveAnimatorParams.IS_WALKING, false);
    }

    public override void UpdateAnt()
    {
        if (Vector3.Distance(followerAnt.transform.position, player.transform.position) <= followerAnt.recruitmentRange)
        {
            followerAnt.recruited = true;
            followerAnt.SetAntState(FollowerAnt.AntState.RECRUITED);
			SoundManager.PlayOneShot(followerAnt.recruitedSound);
            HealthManager.AssignAnt(followerAnt.gameObject);
        }
    }
}