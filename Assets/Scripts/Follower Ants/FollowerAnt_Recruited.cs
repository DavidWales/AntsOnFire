﻿using UnityEngine;

public class FollowerAnt_Recruited : FollowerAnt_State
{
    private FollowerAnt followerAnt;
    private GameObject ant;

    private Animator followerAnimator;

    public FollowerAnt_Recruited(FollowerAnt _followerAnt)
    {
        followerAnt = _followerAnt;
        ant = followerAnt.gameObject;
        followerAnimator = followerAnt.GetComponent<Animator>();
    }

    public override void UpdateAnt()
    {
        Vector3 dir = followerAnt.nextAnt.transform.position - ant.transform.position;
        if (dir != Vector3.zero)
        {
            if (Vector3.Angle(ant.transform.forward, dir) > 1.0f)
            {
                Quaternion lookRot = Quaternion.LookRotation(dir);
                ant.transform.rotation = Quaternion.Lerp(ant.transform.rotation, lookRot, Time.deltaTime * followerAnt.turnSpeed);
            }
            if (Vector3.Distance(ant.transform.position, followerAnt.nextAnt.transform.position) > followerAnt.minDistanceToNextAnt)
            {
                ant.transform.position = Vector3.MoveTowards(ant.transform.position,
                                                            followerAnt.nextAnt.transform.position - (dir.normalized * followerAnt.minDistanceToNextAnt), 
                                                            followerAnt.speed * Time.deltaTime);
                followerAnimator.SetBool(SlaveAnimatorParams.IS_WALKING, true);
            }
            else
            {
                followerAnimator.SetBool(SlaveAnimatorParams.IS_WALKING, false);
            }
        }
    }
}