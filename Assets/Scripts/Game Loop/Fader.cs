﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fader : MonoBehaviour
{
    public float fadeInTime = 2.0f;
    public float fadeOutTime = 2.0f;
    private RawImage fader;

    void Awake()
    {
        fader = this.GetComponent<RawImage>();
    }

    public void FadeOut(Action callback)
    {
        StartCoroutine(FadeOutCoroutine(callback));
    }

    private IEnumerator FadeOutCoroutine(Action callback)
    {
        Color c;
        while(fader.color.a < 1.0f)
        {
            c = fader.color;
            c.a += Time.unscaledDeltaTime * (1.0f / fadeOutTime);
            fader.color = c;
            yield return null;
        }

        c = fader.color;
        c.a = 1.0f;
        fader.color = c;

        callback();
    }

    public void FadeIn(Action callback)
    {
        StartCoroutine(FadeInCoroutine(callback));
    }

    private IEnumerator FadeInCoroutine(Action callback)
    {
        Color c;
        while (fader.color.a > 0.0f)
        {
            c = fader.color;
            c.a -= Time.unscaledDeltaTime * (1.0f / fadeInTime);
            fader.color = c;
            yield return null;
        }

        c = fader.color;
        c.a = 0.0f;
        fader.color = c;

        callback();
    }
}