﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistCanvas : MonoBehaviour
{

    public static PersistCanvas _inst = null;

    void Awake()
    {
        if (_inst != null)
            Destroy(this.gameObject);
        _inst = this;
        DontDestroyOnLoad(this.gameObject);
    }

}