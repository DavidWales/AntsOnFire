﻿using UnityEngine;

public class SceneExit : MonoBehaviour
{

    public SceneIndex sceneIndex;

    void OnTriggerEnter(Collider collider)
    {
        if(collider.GetComponent<PlayerController>() != null)
        {
            Time.timeScale = 0.0f;
            LevelManager.EndLevel(sceneIndex);
        }
    }

}