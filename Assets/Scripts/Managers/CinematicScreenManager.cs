﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CinematicScreenManager : MonoBehaviour
{
	public Image storyImage;
	public Sprite[] storyImages;
	public AudioClip buttonSound;
	private SceneIndex nextLevel = SceneIndex.SPLASH_SCREEN;

	private void SetCinematicImage()
	{
		switch(GameManager.GetLastLevelIndex())
		{
			case SceneIndex.SPLASH_SCREEN:
				storyImage.sprite = storyImages[0];
				break;
			case SceneIndex.LEVEL_1:
				storyImage.sprite = storyImages[1];
				break;
			case SceneIndex.LEVEL_2:
				storyImage.sprite = storyImages[2];
				break;
			case SceneIndex.LEVEL_3:
				storyImage.sprite = storyImages[3];
				break;
			case SceneIndex.LEVEL_4:
				storyImage.sprite = storyImages[4];
				break;
			case SceneIndex.LEVEL_5:
				storyImage.sprite = storyImages[5];
				break;
		}
	}

    public static SceneIndex GetNextLevel()
    {
        switch (GameManager.GetLastLevelIndex())
        {
            case SceneIndex.SPLASH_SCREEN:
                _inst.nextLevel = SceneIndex.LEVEL_1;
                break;
            case SceneIndex.LEVEL_1:
                _inst.nextLevel = SceneIndex.LEVEL_2;
                break;
            case SceneIndex.LEVEL_2:
                _inst.nextLevel = SceneIndex.LEVEL_3;
                break;
            case SceneIndex.LEVEL_3:
                _inst.nextLevel = SceneIndex.LEVEL_4;
                break;
            case SceneIndex.LEVEL_4:
                _inst.nextLevel = SceneIndex.LEVEL_5;
                break;
            case SceneIndex.LEVEL_5:
                _inst.nextLevel = SceneIndex.VICTORY_LOSS_SCREEN;
                break;
        }
        return _inst.nextLevel;
    }

    void Start()
    {
        GameManager.SetSceneStarted();
		SetCinematicImage();
    }

    public void PlayButtonPressed()
    {
		SoundManager.PlayOneShot(buttonSound);
		GameManager.ChangeScene(nextLevel);
    }

    #region Singleton Stuff

    public static CinematicScreenManager _inst = null;

    void Awake()
    {
        if (_inst != null)
        {
            GameObject.Destroy(this.gameObject);
            return;
        }
        _inst = this;
        GetNextLevel();
    }

    #endregion

}