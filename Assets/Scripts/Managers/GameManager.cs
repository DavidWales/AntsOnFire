﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private Fader fader;
    private bool isChanging = false;
    private SceneIndex lastLevelIndex;
    private bool victory = false;

    public static void SetSceneStarted()
    {
        if (_inst.isChanging == false)
        {
            SceneIndex curScene = (SceneIndex)SceneManager.GetActiveScene().buildIndex;
            SoundManager.TryChangeMusic(curScene == SceneIndex.CINEMATIC_SCREEN ? CinematicScreenManager.GetNextLevel() : curScene);
            _inst.isChanging = true;
            _inst.fader.FadeIn(() =>
            {
                _inst.isChanging = false;
                Time.timeScale = 1.0f;
            });
        }
    }

    public static void ChangeScene(SceneIndex sceneIndex)
    {
        if (_inst.isChanging == false)
        {
            _inst.lastLevelIndex = (SceneIndex)SceneManager.GetActiveScene().buildIndex;
            _inst.isChanging = true;
            Time.timeScale = 0.0f;
            _inst.fader.FadeOut(() =>
            {
                SceneManager.LoadScene((int)sceneIndex);
                _inst.isChanging = false;
            });
        }
    }

    public static void SetVictory(bool result)
    {
        _inst.victory = result;
    }

    public static bool GetVictory()
    {
        return _inst.victory;
    }

    void OnApplicationQuit()
    {
        PlayerPrefs.DeleteKey(Prefs.NUM_ANTS);
    }

    public static SceneIndex GetLastLevelIndex()
    {
        return _inst.lastLevelIndex;
    }

    void Start()
    {
        SoundManager.TryChangeMusic((SceneIndex)SceneManager.GetActiveScene().buildIndex);
    }

    #region Singleton Stuff

    public static GameManager _inst = null;

    void Awake()
    {
        if(_inst != null)
        {
            GameObject.Destroy(this.gameObject);
            return;
        }
        _inst = this;
        DontDestroyOnLoad(this.gameObject);
        fader = GameObject.FindObjectOfType<Fader>();
    }

    #endregion

}