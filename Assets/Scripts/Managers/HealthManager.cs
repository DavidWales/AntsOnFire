﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthManager : MonoBehaviour
{
    private PlayerController player;
    private GameObject followerAntsNode;
    public int health = 1;

    public static void AddHealth(int hp, GameObject specificAnt = null, bool destroyAntIfNegative = true)
    {
        _inst.health += hp;

        if(destroyAntIfNegative == true && hp < 0)
        {
            for(int i = 0; i > hp; i--)
            {
                DestroyAnt(specificAnt);
            }
        }

        if(_inst.health <= 0)
        {
			//Game Over
			_inst.player.Die();
            GameManager.SetVictory(false);
            LevelManager.EndLevel(SceneIndex.VICTORY_LOSS_SCREEN);
        }
    }

    public static int GetHealth()
    {
        return _inst.health;
    }

    public static void AssignAnt(GameObject ant)
    {
        FollowerAnt followerAnt = ant.GetComponent<FollowerAnt>();

        GameObject nextAnt = null;
        if (_inst.followerAntsNode.transform.childCount > 0)
        {
            nextAnt = _inst.followerAntsNode.transform.GetChild(_inst.followerAntsNode.transform.childCount - 1).gameObject;
            nextAnt.GetComponent<FollowerAnt>().prevAnt = ant;
        }
        else
        {
            nextAnt = _inst.player.gameObject;
        }

        followerAnt.SetNextAnt(nextAnt);
        followerAnt.transform.parent = _inst.followerAntsNode.transform; //This must be done after you get the last child...
        HealthManager.AddHealth(1);
    }

    public static void DestroyAnt(GameObject specificAnt = null)
    {
        if (specificAnt != null)
        {
            //Reassign Prev Ant to Next Ant
            FollowerAnt followerAnt = specificAnt.GetComponent<FollowerAnt>();
            if (followerAnt.prevAnt != null)
            {
                followerAnt.prevAnt.GetComponent<FollowerAnt>().nextAnt = followerAnt.nextAnt;
            }

            //Inform Next Ant of Prev Ant Change
            FollowerAnt nextFollowerAnt = followerAnt.nextAnt.GetComponent<FollowerAnt>();
            if(nextFollowerAnt != null)
            {
                nextFollowerAnt.prevAnt = followerAnt.prevAnt;
            }

			followerAnt.Die();
        }
        else
        {
            //We shouldn't need to worry about destroying the player
            if (_inst.followerAntsNode.transform.childCount > 0)
            {
                Transform ant = _inst.followerAntsNode.transform.GetChild(_inst.followerAntsNode.transform.childCount - 1);

                FollowerAnt followerAnt = ant.GetComponent<FollowerAnt>();
                FollowerAnt nextFollowerAnt = followerAnt.nextAnt.GetComponent<FollowerAnt>();
                if(nextFollowerAnt != null)
                {
                    nextFollowerAnt.prevAnt = null;
                }

				followerAnt.Die();
            }
        }
    }

    #region Singleton Stuff

    public static HealthManager _inst = null;

    void Awake()
    {
        if (_inst != null)
        {
            GameObject.Destroy(this.gameObject);
            return;
        }
        _inst = this;

        followerAntsNode = GameObject.FindGameObjectWithTag(Tags.FOLLOWER_ANTS_NODE);
        player = GameObject.FindObjectOfType<PlayerController>();
    }

    #endregion
}