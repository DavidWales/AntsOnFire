﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public GameObject followerAntPrefab;
    public int NumberOfInitialFollowerAnts = 2;
    
    private PlayerController player;

    private void SpawnFollowerAnts()
    {
        if (PlayerPrefs.HasKey(Prefs.NUM_ANTS))
        {
            NumberOfInitialFollowerAnts = PlayerPrefs.GetInt(Prefs.NUM_ANTS);
        }

        for (int i = 0; i < NumberOfInitialFollowerAnts; i++)
        {
            FollowerAnt followerAnt = Instantiate(followerAntPrefab, player.transform.position, player.transform.rotation).GetComponent<FollowerAnt>();
            HealthManager.AssignAnt(followerAnt.gameObject);
        }
    }

    public static void StartLevel(Scene scene, LoadSceneMode loadSceneMode)
    {
        _inst.SpawnFollowerAnts();
        GameManager.SetSceneStarted();
    }

    public static void EndLevel(SceneIndex nextScene)
    {
        SceneIndex currScene = (SceneIndex)SceneManager.GetActiveScene().buildIndex;

        if(currScene == SceneIndex.LEVEL_5 && HealthManager.GetHealth() > 0)
        {
            GameManager.SetVictory(true);
        }

        if (currScene != SceneIndex.LEVEL_5 && HealthManager.GetHealth() > 0)
        {
            PlayerPrefs.SetInt(Prefs.NUM_ANTS, HealthManager.GetHealth() - 1);
        }
        else
        {
            PlayerPrefs.DeleteKey(Prefs.NUM_ANTS);
        }
        PlayerPrefs.Save();
        GameManager.ChangeScene(nextScene);
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += StartLevel;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= StartLevel;
    }

    #region Singleton Stuff

    public static LevelManager _inst = null;

    void Awake()
    {
        if (_inst != null)
        {
            GameObject.Destroy(this.gameObject);
            return;
        }
        _inst = this;
        player = GameObject.FindObjectOfType<PlayerController>();
    }

    #endregion
}