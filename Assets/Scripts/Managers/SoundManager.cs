﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

public class SoundManager : MonoBehaviour
{

    private AudioSource audioSource;

    [Serializable]
    public struct SceneMusic
    {
        public SceneIndex sceneIndex;
        public AudioClip music;
    }

    public float volumeFadeOutTime = 2.0f;
    public float volumeFadeInTime = 2.0f;
    public SceneMusic[] sceneMusic;

    public static void TryChangeMusic(SceneIndex sceneIndex)
    {
        AudioClip sceneClip = _inst.sceneMusic.FirstOrDefault(s => s.sceneIndex == sceneIndex).music;
        if (_inst.audioSource.clip != sceneClip)
        {
            if (_inst.audioSource.isPlaying == false)
            {
                _inst.audioSource.clip = sceneClip;
            }
            else
            {
                _inst.FadeOut(() =>
                {
                    _inst.audioSource.clip = sceneClip;
                    _inst.audioSource.Play();
                    _inst.FadeIn(() => { });
                });
            }
        }
    }

    private void FadeOut(Action callback)
    {
        StartCoroutine(FadeMusicOut(callback));
    }

    private IEnumerator FadeMusicOut(Action callback)
    {
        while(audioSource.volume > 0.0f)
        {
            audioSource.volume -= Time.unscaledDeltaTime * (1.0f / volumeFadeOutTime);
            yield return null;
        }
        audioSource.volume = 0.0f;
        callback();
    }

    private void FadeIn(Action callback)
    {
        StartCoroutine(FadeMusicIn(callback));
    }

    private IEnumerator FadeMusicIn(Action callback)
    {
        while (audioSource.volume < 1.0f)
        {
            audioSource.volume += Time.unscaledDeltaTime * (1.0f / volumeFadeInTime);
            yield return null;
        }
        audioSource.volume = 1.0f;
        callback();
    }

	public static void PlayOneShot(AudioClip clip)
	{
		_inst.audioSource.PlayOneShot(clip, 1.0f);
	}

	void Start()
    {
        audioSource.Play();
    }

    #region Singleton Stuff

    public static SoundManager _inst = null;

    void Awake()
    {
        if (_inst != null)
        {
            GameObject.Destroy(this.gameObject);
            return;
        }
        _inst = this;
        DontDestroyOnLoad(this.gameObject);
        audioSource = this.GetComponent<AudioSource>();
    }

    #endregion

}