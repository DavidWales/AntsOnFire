﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashScreenManager : MonoBehaviour
{
	public AudioClip buttonSound;

    void Start()
    {
        GameManager.SetSceneStarted();
        GameManager.SetVictory(false);
    }

    public void PlayButtonPressed()
    {
		SoundManager.PlayOneShot(buttonSound);
		GameManager.ChangeScene(SceneIndex.CINEMATIC_SCREEN);
    }

    public void QuitButtonPressed()
    {
		SoundManager.PlayOneShot(buttonSound);
        Application.Quit();
    }

    #region Singleton Stuff

    public static SplashScreenManager _inst = null;

    void Awake()
    {
        if (_inst != null)
        {
            GameObject.Destroy(this.gameObject);
            return;
        }
        _inst = this;
    }

    #endregion

}