﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinLoseManager : MonoBehaviour
{
    public float screenTime = 7.5f;
    public SceneIndex nextScene = SceneIndex.SPLASH_SCREEN;

    private Image winImage;
    private Image loseImage;

    void Start()
    {
        GameManager.SetSceneStarted();
        winImage = GameObject.Find("Win").GetComponent<Image>();
        loseImage = GameObject.Find("Lose").GetComponent<Image>();

        if(GameManager.GetVictory() == true)
        {
            winImage.enabled = true;
        }
        else
        {
            loseImage.enabled = true;
        }

        StartCoroutine(GoToScene());
    }

    private IEnumerator GoToScene()
    {
        yield return new WaitForSeconds(screenTime);
        GameManager.ChangeScene(nextScene);
    }

    #region Singleton Stuff

    public static WinLoseManager _inst = null;

    void Awake()
    {
        if (_inst != null)
        {
            GameObject.Destroy(this.gameObject);
            return;
        }
        _inst = this;
    }

    #endregion

}