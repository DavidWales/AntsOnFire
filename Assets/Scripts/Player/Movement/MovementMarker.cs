﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementMarker : Trap
{
    protected override void OnTriggerEnterWithAnt(Collider collider)
    {
        base.OnTriggerEnterWithAnt(collider);
        Destroy(this.gameObject);
    }
}