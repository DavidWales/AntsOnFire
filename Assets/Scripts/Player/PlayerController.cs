﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float turnSpeed;
    public GameObject movementMarkerPrefab;
    public int maxMarkers = 5;
	public AudioClip deathSound;

    private GameObject player;
    private Vector3? cellTarget = null;
    private List<GameObject> movementMarkers = new List<GameObject>();
    private Animator playerAnimator;

    void Awake()
    {
        player = this.gameObject;
        playerAnimator = this.GetComponent<Animator>();
    }

    void OnDrawGizmos()
    {
        if(cellTarget.HasValue)
        {
            Gizmos.DrawSphere(cellTarget.Value, 1);
        }
    }

    void Update()
    {
        if(Input.GetMouseButtonDown((int)MouseButton.LEFT_BUTTON))
        {
            SetPositionWithClick();
        }

        if(cellTarget.HasValue)
        {
            if(Vector3.Distance(player.transform.position, cellTarget.Value) < 0.1)
            {
                cellTarget = null;
                playerAnimator.SetBool(PlayerAnimatorParams.IS_WALKING, false);
            }
            else
            {
                Vector3 dir = cellTarget.Value - player.transform.position;
                if (Vector3.Angle(player.transform.forward, dir) > 1.0f)
                {
                    Quaternion lookRot = Quaternion.LookRotation(dir);
                    player.transform.rotation = Quaternion.Lerp(player.transform.rotation, lookRot, Time.deltaTime * turnSpeed);
                }
                player.transform.position = Vector3.MoveTowards(player.transform.position, cellTarget.Value, speed * Time.deltaTime);
            }
        }
    }

    private void SetPositionWithClick()
    {
        List<RaycastHit> raycastHit = Physics.RaycastAll(Camera.main.ScreenPointToRay(Input.mousePosition)).ToList();
        if(raycastHit.Count > 0)
        {
            RaycastHit? firstValidLocation = raycastHit.FirstOrDefault(r => r.transform.GetComponent<GridObject>());
            if(firstValidLocation.HasValue)
            {
                if (movementMarkers.Count > maxMarkers)
                {
                    Destroy(movementMarkers.First());
                    movementMarkers.Remove(movementMarkers.First());
                }
                movementMarkers.Add(Instantiate(movementMarkerPrefab, firstValidLocation.Value.point + (Vector3.up * 3.0f), Quaternion.Euler(90.0f, 0.0f, 0.0f)));
                cellTarget = new Vector3(firstValidLocation.Value.point.x, player.transform.position.y, firstValidLocation.Value.point.z);
                playerAnimator.SetBool(PlayerAnimatorParams.IS_WALKING, true);
            }
        }
    }

	public void Die()
	{
		SoundManager.PlayOneShot(deathSound);
		playerAnimator.SetBool(PlayerAnimatorParams.IS_DYING, true);
	}

}