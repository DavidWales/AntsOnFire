﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnteaterTrap : Trap
{
    private Animator anteaterAnimator;
	public AudioClip lickSound;

    void Awake()
    {
        anteaterAnimator = this.GetComponent<Animator>();
    }

    void Update()
    {
        if(anteaterAnimator.GetBool(AnteaterAnimatorParams.IS_ATTACKING) == true &&
            anteaterAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
        {
            anteaterAnimator.SetBool(AnteaterAnimatorParams.IS_ATTACKING, false);
        }
    }

    public void Attack()
    {
        anteaterAnimator.SetBool(AnteaterAnimatorParams.IS_ATTACKING, true);
		SoundManager.PlayOneShot(lickSound);
    }

    protected override void OnTriggerEnterWithAnt(Collider collider)
    {
        base.OnTriggerEnterWithAnt(collider);
        if (anteaterAnimator.GetBool(AnteaterAnimatorParams.IS_ATTACKING) == true)
        {
            if (collider.GetComponent<FollowerAnt>())
            {
                HealthManager.AddHealth(-1, collider.gameObject);
            }
            else
            {
                HealthManager.AddHealth(-1);
            }
        }
    }
}