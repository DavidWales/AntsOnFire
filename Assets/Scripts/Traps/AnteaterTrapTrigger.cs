﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnteaterTrapTrigger : Trap
{
    public AnteaterTrap anteaterTrap;
    public Vector2 triggerTimeRange = new Vector2(5.0f, 10.0f);

    protected override void OnTriggerEnterWithAnt(Collider collider)
    {
        base.OnTriggerEnterWithAnt(collider);
        StartCoroutine(TriggerAttack());
    }

    private IEnumerator TriggerAttack()
    {
        yield return new WaitForSeconds(Random.Range(triggerTimeRange.x, triggerTimeRange.y));
        anteaterTrap.Attack();
    }
}