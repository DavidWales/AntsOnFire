﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FireTrap : Trap
{
    public bool isStatic = false;
    public int damage = -1;
    public float fireSpawnTimer = 0.1f;
    public float spreadChancePercent = 70.0f;
    public float spreadSpeed = 0.01f;

    private GameObject fire;
    private BoxCollider fireCollider;
    private ParticleSystem fireParticle;
    private float fireDensityPerArea;

    private ParticleSystem smokeParticle;
    private float smokeDensityPerArea;

    private Dictionary<Vector3, bool> canSpreadInDirection = new Dictionary<Vector3, bool>
    {
        { Vector3.forward, true },
        { Vector3.back, true },
        { Vector3.left, true },
        { Vector3.right, true }
    };

    void Start()
    {
        if (isStatic == false)
        {
            fireCollider = this.GetComponent<BoxCollider>();
            fire = this.gameObject;

            fireParticle = this.transform.GetChild(0).GetComponent<ParticleSystem>();
            fireDensityPerArea = fireParticle.main.maxParticles / fireParticle.shape.scale.magnitude;

            smokeParticle = fireParticle.transform.GetChild(0).GetChild(0).GetComponent<ParticleSystem>();
            smokeDensityPerArea = smokeParticle.main.maxParticles / smokeParticle.shape.scale.magnitude;

            StartCoroutine(SpreadFire());
        }
    }

    private void ExpandInDirection(Vector3 direction, float distance)
    {
        Vector3 dir = direction * distance;
        dir = new Vector3(Mathf.Abs(dir.x), Mathf.Abs(dir.y), Mathf.Abs(dir.z));
        fireCollider.size += dir;
        fire.transform.position += direction * (distance / 2);

        //Set Fire Settings
        ParticleSystem.ShapeModule particleShape = fireParticle.shape;
        particleShape.scale += dir;

        ParticleSystem.MainModule fireMain = fireParticle.main;
        fireMain.maxParticles = (int)(fireDensityPerArea * fireParticle.shape.scale.magnitude);

        ParticleSystem.EmissionModule fireEmission = fireParticle.emission;
        fireEmission.rateOverTime = fireMain.maxParticles;

        //Set Smoke Settings
        ParticleSystem.ShapeModule smokeShape = smokeParticle.shape;
        smokeShape.scale = fireParticle.shape.scale;

        ParticleSystem.MainModule smokeMain = smokeParticle.main;
        smokeMain.maxParticles = (int)(smokeDensityPerArea * smokeParticle.shape.scale.magnitude);

        ParticleSystem.EmissionModule smokeEmission = smokeParticle.emission;
        smokeEmission.rateOverTime = smokeMain.maxParticles;
    }

    private IEnumerator SpreadFire()
    {
        while (true)
        {
            yield return new WaitForSeconds(fireSpawnTimer);

            TrySpreadFire(Vector3.forward, spreadSpeed * Time.deltaTime);
            TrySpreadFire(Vector3.back, spreadSpeed * Time.deltaTime);
            TrySpreadFire(Vector3.left, spreadSpeed * Time.deltaTime);
            TrySpreadFire(Vector3.right, spreadSpeed * Time.deltaTime);

            if (canSpreadInDirection.ContainsValue(true) == false)
            {
                break;
            }
        }
    }

    private void TrySpreadFire(Vector3 direction, float distance)
    {
        if (canSpreadInDirection[direction] == false)
            return;

        if (Random.Range(0.0f, 1.0f) <= (spreadChancePercent / 100.0f))
        {
            Vector3 halfCollisionBounds = fireCollider.bounds.size / 2.0f;
            Vector3 spawnPosition = (fire.transform.position 
                                    + new Vector3(  direction.x * halfCollisionBounds.x,
                                                    direction.y * halfCollisionBounds.y, 
                                                    direction.z * halfCollisionBounds.z) 
                                    + (direction * distance));

            //Check to see if there is ground underneath the spawn point
            List<RaycastHit> hitsFromFireSpawn = Physics.RaycastAll(spawnPosition, Vector3.down, fireCollider.bounds.size.y * 2.0f).ToList();
            if (!hitsFromFireSpawn.Any(hfs => hfs.collider.GetComponent<GridObject>()))
            {
                canSpreadInDirection[direction] = false;
                return;
            }

            ExpandInDirection(direction, distance);
        }
    }

    protected override void OnTriggerEnterWithAnt(Collider collider)
    {
        base.OnTriggerEnterWithAnt(collider);

        GameObject ant = null;
        if(collider.GetComponent<FollowerAnt>())
        {
            ant = collider.gameObject;
        }
        HealthManager.AddHealth(damage, ant);
    }
}