﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlueTrap : Trap
{
    public float speedReductionPercent = 0.5f;
	public AudioClip glueSound;

    protected override void OnTriggerEnterWithAnt(Collider collider)
    {
        base.OnTriggerEnterWithAnt(collider);
        PlayerController player = collider.GetComponent<PlayerController>();
        if(player != null)
        {
            player.speed *= speedReductionPercent;
			SoundManager.PlayOneShot(glueSound);
        }
        else
        {
            FollowerAnt followerAnt = collider.GetComponent<FollowerAnt>();
            if(followerAnt != null)
            {
                followerAnt.speed *= speedReductionPercent;
				SoundManager.PlayOneShot(glueSound);
			}
        }
    }

    protected override void OnTriggerExitWithAnt(Collider collider)
    {
        base.OnTriggerExitWithAnt(collider);
        PlayerController player = collider.GetComponent<PlayerController>();
        if (player != null)
        {
            player.speed /= speedReductionPercent;
        }
        else
        {
            FollowerAnt followerAnt = collider.GetComponent<FollowerAnt>();
            if (followerAnt != null)
            {
                followerAnt.speed /= speedReductionPercent;
            }
        }
    }
}