﻿using UnityEngine;

abstract public class Trap : MonoBehaviour
{
    protected virtual void OnCollisionEnterWithAnt(Collider collider)
    {

    }

    protected virtual void OnTriggerEnterWithAnt(Collider collider)
    {

    }

    void OnTriggerEnter(Collider collider)
    {
        if(collider.GetComponent<PlayerCollisionEntity>() != null)
        {
            OnCollisionEnterWithAnt(collider);
            OnTriggerEnterWithAnt(collider);
        }
    }

    protected virtual void OnCollisionExitWithAnt(Collider collider)
    {

    }

    protected virtual void OnTriggerExitWithAnt(Collider collider)
    {

    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.GetComponent<PlayerCollisionEntity>() != null)
        {
            OnCollisionExitWithAnt(collider);
            OnTriggerExitWithAnt(collider);
        }
    }
}